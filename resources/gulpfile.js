const gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create();


gulp.task('sass', function () {
    gulp.src('sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix({
        browsers: ['last 2 versions', '> 5%', 'ie 8']
    }))
    .pipe(gulp.dest('../css/'))
    .pipe(browserSync.reload({stream: true}))
});


gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: '.././'
        }
    });

    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('../index.html').on('change', browserSync.reload);

});


gulp.task('default', ['sass', 'server']);
