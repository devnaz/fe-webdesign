function bindEvents() {
    // Show search input
    $('#search-btn').on('click', function() {
        $('#search-text').toggleClass('active');
    });
}

function scrollPages() {
    var header = $('#header');

    // Scroll Header
    $(window).scroll(function() {
        var h = $(this).scrollTop();
        if(h > 0) header.addClass('active');
        else header.removeClass('active');
    });

    // Scroll on links of menu
    $("#header-menu").on("click","a", function(event) {
		event.preventDefault();

		var id  = $(this).attr('href'),
		top = $(id).offset().top;

		$('body,html').animate({scrollTop: top}, 500);
	});

    // Scroll down
    $("#down").on("click", function(event) {
		event.preventDefault();

		$('body,html').animate({scrollTop: $('#home').height()}, 500);
	});
}

// Popup
function popup() {
    var wrapPopup = $('#wrap-popup'),
        body = $('body');

    $('#close-popup').on('click', function(event) {
        event.preventDefault();
        wrapPopup.fadeOut(300);
        body.css('overflow', 'auto');
    });

    $(document).click(function(event) {
        if( event.target == wrapPopup[0]) {
            wrapPopup.fadeOut(300);
            body.css('overflow', 'auto');
        }
    });

    $('.js-popup').each(function() {
        $(this).on('click', function() {
            body.css('overflow', 'hidden');
            wrapPopup.fadeIn(300);
        })
    })
}

window.onload = function() {
    scrollPages();
    bindEvents();
    popup();
}
